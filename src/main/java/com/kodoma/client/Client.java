package com.kodoma.client;

import org.glassfish.tyrus.client.ClientManager;

import javax.websocket.Session;
import java.net.URI;
import java.util.Scanner;

import static com.kodoma.util.JsonUtil.formatMessage;


public class Client {

    private static final String SERVER = "wss://192.168.56.2/restservice/fxChat";
    // var socket = new WebSocket("wss://192.168.56.2/restservice/fxChat");

    public static void main(String[] args) throws Exception {
        ClientManager client = ClientManager.createClient();
        String message;

        // connect to server
        Scanner scanner = new Scanner(System.in);
        // org.apache.tomcat.websocket.pojo.PojoEndpoint.methodMapping
        System.out.println("Welcome to Tiny Chat!");
        System.out.println("What's your name?");
        String user = scanner.nextLine();
        Session session = client.connectToServer(ClientEndpoint.class, new URI(SERVER));
        System.out.println("You are logged in as: " + user);

        // repeatedly read a message and send it to the server (until quit)
        do {
            message = scanner.nextLine();
            session.getBasicRemote().sendText(formatMessage(message, user));
        }
        while (!message.equalsIgnoreCase("quit"));
    }
}