package com.kodoma.client;

import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.WebSocketAdapter;
import org.eclipse.jetty.websocket.client.ClientUpgradeRequest;
import org.eclipse.jetty.websocket.client.WebSocketClient;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.net.URI;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.Future;

/**
 * FXWebSocketClient class.
 * Created on 02.12.2018.
 * @author Kodoma.
 */
public class FXWebSocketClient {

    public static class FXWebSocket extends WebSocketAdapter {

        @Override
        public void onWebSocketText(String message) {
            try {
                // echo the message
                getRemote().sendString(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        try {
            new FXWebSocketClient().demo();
        } catch (Throwable t) {
            t.printStackTrace(System.err);
        }
    }

    public void demo() throws Exception {
        final WebSocketClient client = new WebSocketClient(new SslContextFactory());
        try {
            trustAllHosts(client);
            client.start();

            ClientUpgradeRequest request = new ClientUpgradeRequest();
            request.setSubProtocols("xsCrossfire");
            request.setHeader("Authorization", "Basic TLVWQMZqRr2hasYnZoI=");

            URI wsUri = URI.create("wss://192.168.56.2:8443/restservice/fxChat");

            final FXWebSocket socket = new FXWebSocket();
            final Future<Session> future = client.connect(socket, wsUri, request);

            future.get(); // wait for connect

            System.out.println("Success connect");

            socket.getRemote().sendString("hello"); // send message
        } finally {
            client.stop();
        }
    }

    public static void trustAllHosts(final WebSocketClient client) {
        // Create a trust manager that does not validate certificate chains
        final TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {

                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return new java.security.cert.X509Certificate[] {
                        };
                    }

                    public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                    }
                }
        };

        // Install the all-trusting trust manager
        try {
            final SSLContext sc = SSLContext.getInstance("TLS");

            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            client.getSslContextFactory().setSslContext(sc);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
