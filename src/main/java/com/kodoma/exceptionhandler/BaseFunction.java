package com.kodoma.exceptionhandler;

import com.google.common.collect.Lists;
import com.kodoma.server.FXServer;

import java.util.Collections;
import java.util.List;

/**
 * BaseFunction class.
 * Created on 28.10.2018.
 * @author Kodoma.
 */
public interface BaseFunction {

    List<FXServer> RESOURCES = Lists.newArrayList();

    static void invoke(ThrowingVoidFunction function) {
        function.apply();
    }

    static <T, R> R invoke(ThrowingFunction<T, R> function) {
        // Можем вызвать с null, т.к. значение перезапишется во время вызова метода applyThrows
        return function.apply(null);
    }

    static void setResource(FXServer... servers) {
        Collections.addAll(RESOURCES, servers);
    }
}
