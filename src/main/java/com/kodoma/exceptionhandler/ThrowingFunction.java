package com.kodoma.exceptionhandler;

import com.kodoma.server.FXServer;

import java.util.Objects;

/**
 * ThrowingFunction class.
 * Created on 28.10.2018.
 * @author Kodoma.
 */
@FunctionalInterface
public interface ThrowingFunction<T, R> extends BaseFunction {

    R applyThrows(T t) throws Exception;

    default R apply(T t) {
        try {
            return applyThrows(t);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (!RESOURCES.isEmpty()) {
                RESOURCES.stream().filter(Objects::nonNull).forEach(FXServer::close);
            }
        }
    }
}