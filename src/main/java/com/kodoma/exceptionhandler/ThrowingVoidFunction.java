package com.kodoma.exceptionhandler;

/**
 * ThrowingVoidFunction class.
 * Created on 28.10.2018.
 * @author Kodoma.
 */
@FunctionalInterface
public interface ThrowingVoidFunction extends BaseFunction {

    void applyThrows() throws Exception;

    default void apply() {
        try {
            applyThrows();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}