package com.kodoma;

import com.kodoma.server.FXServerEndpoint;
import com.kodoma.server.FXTomcatServer;

/**
 * Test class.
 * Created on 01.12.2018.
 * @author Kodoma.
 */
public class Test {

    public static void main(String[] args) {
        FXTomcatServer.get("localhost", 8003, "/ws", FXServerEndpoint.class)
                      .setFile("./opt/tomcat/logs/fxserver.log")
                      .start();
    }
}
