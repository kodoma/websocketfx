package com.kodoma.server;

import org.glassfish.tyrus.server.Server;

import java.util.Scanner;

import static com.kodoma.exceptionhandler.BaseFunction.invoke;

public class FXGlassfishServer {

    public static void main(String[] args) {
        final Server server = new Server("localhost", 8025, "/ws", FXServerEndpoint.class);

        try {
            invoke(server::start);
            System.out.println("Press any key to stop the server..");
            new Scanner(System.in).nextLine();
        } finally {
            server.stop();
        }
    }
}
