package com.kodoma.server;

import com.google.common.collect.Sets;
import com.kodoma.exceptionhandler.BaseFunction;
import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;
import org.apache.tomcat.websocket.server.Constants;
import org.apache.tomcat.websocket.server.WsContextListener;

import javax.servlet.ServletContextEvent;
import javax.websocket.server.ServerContainer;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Set;

import static com.kodoma.exceptionhandler.BaseFunction.invoke;

/**
 * FXTomcatServer class.
 * Created on 26.10.2018.
 * @author Kodoma.
 */
public class FXTomcatServer extends FXServer<Tomcat> {

    private static final File DOC_BASE = new File(System.getProperty("java.io.tmpdir"));
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy.MM.dd/HH:mm:ss");
    private static final Set<Tomcat> SERVERS = Collections.synchronizedSet(Sets.newHashSet());

    private static Class srvEndpointClass;
    private static File logFile;

    private FXTomcatServer(String address, int port, String path, Class srvEndPClass) {
        this.address = address;
        this.port = port;
        this.path = path;
        srvEndpointClass = srvEndPClass;
    }

    public static FXTomcatServer get(String address, int port, String path, Class srvEndPClass) {
        return new FXTomcatServer(address, port, path, srvEndPClass);
    }

    public static class Config extends WsContextListener {

        @Override
        public void contextInitialized(final ServletContextEvent sce) {
            super.contextInitialized(sce);
            final ServerContainer sc = (ServerContainer)sce.getServletContext().getAttribute(
                    Constants.SERVER_CONTAINER_SERVLET_CONTEXT_ATTRIBUTE);

            invoke(() -> sc.addEndpoint(srvEndpointClass));
        }
    }

    @Override
    void startServer() {
        if (SERVERS.size() > 1) {
            logMessage("Maximum count of SERVERS");
            return;
        }
        final Tomcat server = new Tomcat();

        setResource(server);
        BaseFunction.setResource(this);
        server.setHostname(address);
        server.setPort(port);
        server.getHost().setAppBase(".");

        final Context context = server.addContext(path, DOC_BASE.getAbsolutePath());

        context.addApplicationListener(Config.class.getName());
        Tomcat.initWebappDefaults(context);
        server.enableNaming();

        logMessage("Preparing server for start");
        invoke(server::init);
        invoke(server::start);

        SERVERS.add(server);

        logMessage("Server is started on address: " + address + ", on port: " + port + ", with path: " + path);
        server.getServer().await();
        logMessage("Client join to server on address: " + address + ", on port: " + port);
    }

    private static void logMessage(final String... text) {
        if (text != null) {
            try (PrintStream out = new PrintStream(new FileOutputStream(logFile, true), true)) {
                Arrays.stream(text).forEach(x -> out.print(DATE_FORMAT.format(Calendar.getInstance().getTime()) + " - " + x + "\r\n"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void close() {
        invoke(resource::stop);
    }

    public static void stopAllServers() {
        EXECUTOR.execute(() -> {
            for (Tomcat server : SERVERS) {
                invoke(server::stop);
                SERVERS.remove(server);
                logMessage("Server " + server.getServer().getAddress() + " is stopped");
            }
        });
    }

    public FXTomcatServer setFile(String logFile) {
        FXTomcatServer.logFile = new File(logFile);
        return this;
    }
}
