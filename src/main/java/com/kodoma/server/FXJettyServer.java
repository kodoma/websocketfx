package com.kodoma.server;


import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.websocket.jsr356.server.ServerContainer;
import org.eclipse.jetty.websocket.jsr356.server.deploy.WebSocketServerContainerInitializer;

import java.util.concurrent.Executors;

/**
 * FXJettyServer class.
 * Created on 26.10.2018.
 * @author dmsokol.
 */
public class FXJettyServer {

    private String address;
    private int port;
    private String path;
    private Class serverEndpointClass;

    private FXJettyServer(String address, int port, String path, Class serverEndpointClass) {
        this.address = address;
        this.port = port;
        this.path = path;
        this.serverEndpointClass = serverEndpointClass;
    }

    // final FXJettyServer fxServer = new FXJettyServer("127.0.0.1", 8025, "/ws", FXServerEndpoint.class);

    public static void main(String[] args) {
        final FXJettyServer fxServer = new FXJettyServer("localhost", 8025, "/ws", FXServerEndpoint.class);
        fxServer.start();
    }

    public void start() {
        Executors.newSingleThreadExecutor().execute(this::startServerExceptionHandler);
    }

    private void startServerExceptionHandler() {
        try {
            startServer();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startServer() throws Exception {
        Server server = null;
        try {
            // Server
            server = new Server();
            final ServerConnector connector = new ServerConnector(server);

            connector.setHost(address);
            connector.setPort(port);
            server.addConnector(connector);

            // ServletContextHandler
            final ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);

            context.setContextPath(path);
            server.setHandler(context);

            // ServerContainer
            final ServerContainer container = WebSocketServerContainerInitializer.configureContext(context);
            container.addEndpoint(serverEndpointClass);

            // Server
            server.start();
            server.join();

        } finally {
            if (server != null) {
                server.stop();
            }
        }
    }
}
