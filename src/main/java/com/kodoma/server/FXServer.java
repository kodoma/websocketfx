package com.kodoma.server;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * FXServer class.
 * Created on 31.10.2018.
 * @author Kodoma.
 */
public abstract class FXServer<T> {

    protected final static ExecutorService EXECUTOR = Executors.newFixedThreadPool(3);

    protected T resource;
    protected String address;
    protected int port;
    protected String path;
    protected Class srvEndpointClass;

    public void start() {
        EXECUTOR.execute(this::startServer);
    }

    abstract void startServer();

    public abstract void close();

    public void setResource(T resource) {
        this.resource = resource;
    }
}
